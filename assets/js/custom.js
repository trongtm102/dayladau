$(document).ready(function() {
  $('.filter_location_label').on('click', function() {
    $('.home_location_dropdown').toggleClass('home_location_dropdown__hidden');
    $('.home_date_dropdown').addClass('home_date_dropdown__hidden');
    $('.home_guests_dropdown').addClass('home_guests_dropdown__hidden');
  });

  $('.home_date_filter-input').on('click', function() {
    $('.home_date_dropdown').toggleClass('home_date_dropdown__hidden');
    $('.home_location_dropdown').addClass('home_location_dropdown__hidden');
    $('.home_guests_dropdown').addClass('home_guests_dropdown__hidden');
  });

  $('.home_guests_input').on('click', function() {
    $('.home_guests_dropdown').toggleClass('home_guests_dropdown__hidden');
    $('.home_date_dropdown').addClass('home_date_dropdown__hidden');
    $('.home_location_dropdown').addClass('home_location_dropdown__hidden');
  });

  $('.btn-clone-popup, .search-btn').on('click', function() { 
    $('.home_guests_dropdown').toggleClass('home_guests_dropdown__hidden');
    $('.home_date_dropdown').addClass('home_date_dropdown__hidden');
    $('.home_location_dropdown').addClass('home_location_dropdown__hidden');
  });

  $('.mymodal-popou').hide();
  $('.listings_filter_checkbox').on('click', function() {
    $('.mymodal-popou').show();
  });

  $('.mymodal_close').on('click', function() {
    $('.mymodal-popou').hide();
  });

  $('.login_menu').on('click', function() {
    $('.dropdown-menu').show();
  });

  let defaultVal = 0;
  
  if (defaultVal >= 0) {
    $('.number_picker__btn .cp').on('click', function() {
      defaultVal++;
      $('.number_picker__number').text(defaultVal);
      let htmlInput = defaultVal + "<div class='text-muted'>&nbsp;người</div>";
      $('.home_guests_input').html(htmlInput);
    });

    $('.minus_picker__btn').on('click', function() {
      let number = $('.number_picker__number').text();
      console.log(number);
      // if ($('.number_picker__number').text)
      // defaultVal--;
      // $('.number_picker__number').text(defaultVal);
      // let htmlInput = defaultVal + "<div class='text-muted'>&nbsp;người</div>";
      // $('.home_guests_input').html(htmlInput);
    });
  } 

  var swiper = 
     new Swiper('.swiper',{
       slidesPerView: '1',
       spaceBetween: 0,
       autoplay: false,
       slidesPerView: 'auto',
       coverflowEffect: {
         slideShadows : true,
       },
       pagination: {
         el: '.swiper-pagination',
       },
     }
    );

    var windowsize = $(window).width();

  $(window).resize(function() {
    windowsize = $(window).width();
    var arg = ["max-width: unset",
                'width: 100%'];
    if (windowsize < 1024) {
      $('.dayladau .root').removeClass('desktop');
      $('.dayladau .root').addClass('mobile');

      $('.dayladau .root .mymodal_modal').css('padding-top', '120px');
      $('.menu-swipper').addClass('content__feature');
      $('.menu-swipper').css(arg);
    } else {
      $('.dayladau .root').removeClass('mobile');
      $('.dayladau .root').addClass('desktop');
      $('.dayladau .root .mymodal_modal').css('padding-top', 'unset');
    }
  });
  $('.filter-by-price .btn').on('click', function() {
    $(this).closest('.filter-by-price').find('.btn').removeClass('btn-primary').addClass('btn-outline-primary');
    $(this).addClass('btn-primary').removeClass('btn-outline-primary');
  })

  $('.btn-reset').on('click', function() {
    $(this).closest('.mymodal_modal').find('input[type="checkbox"]').prop("checked", false);
    $(this).closest('.mymodal_modal').find('.number_picker .number_picker__number').text(1);
    $(this).closest('.mymodal_modal').find('.number_picker__btn:not(.cp) svg').attr('color', 'black');
  })

  $('.number_picker').on("click", '.number_picker__btn', function() {
    var $qty = $(this).closest('.number_picker').find('.number_picker__number');
    var current = parseInt($qty.text());
    if ($(this).hasClass('cp')) {
      $qty.text(current+1);
      $(this).closest('.number_picker').find('.number_picker__btn:not(.cp) svg').attr('color', 'black');
    } else {
      if (current == 1) {
        $(this).closest('.number_picker').find('.number_picker__btn:not(.cp) svg').attr('color', '#D0D3D3');
      }
      if ( current > 0 ) {
        $qty.text(current-1);
      } else {
        $qty.text(0);
         
      }
  }
  });

  // $('.venobox').venobox();
  // $('input[name="daterange"]').daterangepicker({
  //   opens: 'left'
  // }, function(start, end, label) {
  //   console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  // });

$('.ranger-date-picker').daterangepicker({
    "showDropdowns": true,
    "showWeekNumbers": true,
    "showISOWeekNumbers": true,
    "timePicker": true,
    "timePicker24Hour": true,
    "startDate": "01/06/2023",
    "endDate": "01/12/2023",
    "drops": "auto"
  }).on('apply.daterangepicker', function (ev, picker) {
    var startDate = picker.startDate.format('DD');
    var startMonth = picker.startDate.format('MM');

     var endDate = picker.endDate.format('DD');
    var endMonth = picker.endDate.format('MM');
    $('.dateranger-act b').text(startDate + ' thg ' + startMonth + ' - ' + endDate + ' thg ' + endMonth) ;
  });


    $('.dateranger-act').daterangepicker({
    "showDropdowns": true,
    "showWeekNumbers": true,
    "showISOWeekNumbers": true,
    "timePicker": true,
    "timePicker24Hour": true,
    "startDate": "01/06/2023",
    "endDate": "01/12/2023",
    "drops": "auto"
  }).on('apply.daterangepicker', function (ev, picker) {
    var startDate = picker.startDate.format('DD');
    var startMonth = picker.startDate.format('MM');

     var endDate = picker.endDate.format('DD');
    var endMonth = picker.endDate.format('MM');
    $('.dateranger-act b').text(startDate + ' thg ' + startMonth + ' - ' + endDate + ' thg ' + endMonth) ;
  });
  $('.ranger-date-picker').click();
});